# Andromeda

## Quick start
Let's enable the Andromeda overlay.
```bash
# Install eselect-repository if you don't already have it
emerge app-eselect/eselect-repository
eselect repository add andromeda git https://gitlab.com/Alxhr0/andromeda.git
```
