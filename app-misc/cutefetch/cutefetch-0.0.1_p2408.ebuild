# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Simple information system script"
HOMEPAGE="https://github.com/alphatechnolog/cutefetch"

if [[ ${PV} == 9999 ]]; then
  inherit git-r3
  EGIT_REPO_URI="https://github.com/alphatechnolog/cutefetch.git"
else
  MY_COMMIT="865499342fc687bfd3dee22233352f53e9e84e45"
  SRC_URI="https://github.com/alphatechnolog/cutefetch/archive/${MY_COMMIT}.tar.gz -> ${P}.tar.gz"
  S="${WORKDIR}/${PN}-${MY_COMMIT}"
  KEYWORDS="~amd64"
fi

LICENSE="all-rights-reserved"
SLOT="0"

src_install() {
  insinto /usr/bin
  dobin cutefetch
}
