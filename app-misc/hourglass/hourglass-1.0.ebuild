# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Autogenerated by pycargoebuild 0.13.5

EAPI=8

CRATES="
	android-tzdata@0.1.1
	android_system_properties@0.1.5
	anstream@0.6.18
	anstyle-parse@0.2.6
	anstyle-query@1.1.2
	anstyle-wincon@3.0.6
	anstyle@1.0.10
	autocfg@1.4.0
	bumpalo@3.16.0
	cc@1.2.1
	cfg-if@1.0.0
	chrono@0.4.38
	clap@4.5.23
	clap_builder@4.5.23
	clap_derive@4.5.18
	clap_lex@0.7.4
	colorchoice@1.0.3
	colored@2.1.0
	core-foundation-sys@0.8.7
	derive@1.0.0
	heck@0.5.0
	iana-time-zone-haiku@0.1.2
	iana-time-zone@0.1.61
	is_terminal_polyfill@1.70.1
	js-sys@0.3.72
	lazy_static@1.5.0
	libc@0.2.164
	log@0.4.22
	num-traits@0.2.19
	once_cell@1.20.2
	proc-macro2@1.0.92
	quote@1.0.37
	shlex@1.3.0
	strsim@0.11.1
	syn@2.0.89
	unicode-ident@1.0.14
	utf8parse@0.2.2
	wasm-bindgen-backend@0.2.95
	wasm-bindgen-macro-support@0.2.95
	wasm-bindgen-macro@0.2.95
	wasm-bindgen-shared@0.2.95
	wasm-bindgen@0.2.95
	windows-core@0.52.0
	windows-sys@0.48.0
	windows-sys@0.59.0
	windows-targets@0.48.5
	windows-targets@0.52.6
	windows_aarch64_gnullvm@0.48.5
	windows_aarch64_gnullvm@0.52.6
	windows_aarch64_msvc@0.48.5
	windows_aarch64_msvc@0.52.6
	windows_i686_gnu@0.48.5
	windows_i686_gnu@0.52.6
	windows_i686_gnullvm@0.52.6
	windows_i686_msvc@0.48.5
	windows_i686_msvc@0.52.6
	windows_x86_64_gnu@0.48.5
	windows_x86_64_gnu@0.52.6
	windows_x86_64_gnullvm@0.48.5
	windows_x86_64_gnullvm@0.52.6
	windows_x86_64_msvc@0.48.5
	windows_x86_64_msvc@0.52.6
"

inherit cargo

DESCRIPTION="Check the age of a Linux system, maybe even a *BSD one"
HOMEPAGE=""
SRC_URI="
	${CARGO_CRATE_URIS}
	https://gitlab.com/Alxhr0/hourglass/-/archive/${PV}/hourglass-${PV}.tar.gz
"

LICENSE="GPL-3+"
# Dependent crate licenses
LICENSE+=" Apache-2.0 MIT MPL-2.0 Unicode-3.0"
SLOT="0"
KEYWORDS="~amd64"
