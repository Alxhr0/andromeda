# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Autogenerated by pycargoebuild 0.13.3

EAPI=8

CRATES="
	aho-corasick@1.1.3
	android-tzdata@0.1.1
	android_system_properties@0.1.5
	anstream@0.6.15
	anstyle-parse@0.2.5
	anstyle-query@1.1.1
	anstyle-wincon@3.0.4
	anstyle@1.0.8
	autocfg@1.3.0
	bindgen@0.69.4
	bitflags@1.3.2
	bitflags@2.6.0
	block@0.1.6
	bumpalo@3.16.0
	byteorder@1.5.0
	cc@1.1.11
	cexpr@0.6.0
	cfg-if@0.1.10
	cfg-if@1.0.0
	chrono@0.4.38
	clang-sys@1.8.1
	clap@4.5.15
	clap_builder@4.5.15
	clap_derive@4.5.13
	clap_lex@0.7.2
	cocoa@0.20.2
	colorchoice@1.0.2
	colored@2.1.0
	core-foundation-sys@0.7.0
	core-foundation-sys@0.8.7
	core-foundation@0.7.0
	core-foundation@0.9.4
	core-graphics-types@0.1.3
	core-graphics@0.19.2
	core-graphics@0.23.2
	core-video-sys@0.1.4
	crossbeam-deque@0.8.5
	crossbeam-epoch@0.9.18
	crossbeam-utils@0.8.20
	dirs-sys@0.4.1
	dirs@5.0.1
	dlib@0.5.2
	dotenvy@0.15.7
	dunce@1.0.5
	either@1.13.0
	enum-as-inner@0.6.0
	equivalent@1.0.1
	errno@0.3.9
	foreign-types-macros@0.2.3
	foreign-types-shared@0.1.1
	foreign-types-shared@0.3.1
	foreign-types@0.3.2
	foreign-types@0.5.0
	futures-channel@0.3.30
	futures-core@0.3.30
	futures-executor@0.3.30
	futures-io@0.3.30
	futures-macro@0.3.30
	futures-sink@0.3.30
	futures-task@0.3.30
	futures-util@0.3.30
	futures@0.3.30
	gethostname@0.3.0
	getrandom@0.2.15
	glob@0.3.1
	handle-error@0.1.2
	hashbrown@0.14.5
	heck@0.4.1
	heck@0.5.0
	hermit-abi@0.3.9
	home@0.5.9
	iana-time-zone-haiku@0.1.2
	iana-time-zone@0.1.60
	if-addrs@0.10.2
	indexmap@2.4.0
	is_terminal_polyfill@1.70.1
	itertools@0.11.0
	itertools@0.12.1
	js-sys@0.3.70
	lazy_static@1.5.0
	lazycell@1.3.0
	libc@0.2.155
	libloading@0.8.5
	libmacchina@7.3.0
	libredox@0.1.3
	linux-raw-sys@0.4.14
	local-ip-address@0.5.7
	log@0.4.22
	mach2@0.4.2
	malloc_buf@0.0.6
	memchr@2.7.4
	memoffset@0.7.1
	metal@0.18.0
	minimal-lexical@0.2.1
	neli-proc-macros@0.1.3
	neli@0.6.4
	nix@0.26.4
	nom@7.1.3
	ntapi@0.4.1
	num-traits@0.2.19
	num_cpus@1.16.0
	objc@0.2.7
	objc_exception@0.1.2
	once_cell@1.19.0
	option-ext@0.2.0
	os-release@0.1.0
	pciid-parser@0.6.3
	pciid-parser@0.7.2
	piglog@1.4.1
	pin-project-lite@0.2.14
	pin-utils@0.1.0
	pkg-config@0.3.30
	prettyplease@0.2.20
	proc-macro2@1.0.86
	quote@1.0.36
	rayon-core@1.12.1
	rayon@1.10.0
	redox_users@0.4.5
	reference-counted-singleton@0.1.4
	regex-automata@0.4.7
	regex-syntax@0.8.4
	regex@1.10.6
	rpm-pkg-count@0.2.1
	rustc-hash@1.1.0
	rustix@0.38.34
	same-file@1.0.6
	secfmt@0.1.1
	selinux-sys@0.6.10
	selinux@0.4.5
	serde@1.0.207
	serde_derive@1.0.207
	serde_spanned@0.6.7
	shlex@1.3.0
	slab@0.4.9
	sqlite3-src@0.6.1
	sqlite3-sys@0.17.0
	sqlite@0.36.1
	strsim@0.11.1
	syn@1.0.109
	syn@2.0.74
	sysctl@0.5.5
	sysinfo@0.31.2
	thiserror-impl@1.0.63
	thiserror@1.0.63
	toml@0.8.19
	toml_datetime@0.6.8
	toml_edit@0.22.20
	tracing-attributes@0.1.27
	tracing-core@0.1.32
	tracing@0.1.40
	unicode-ident@1.0.12
	utf8parse@0.2.2
	walkdir@2.5.0
	wasi@0.11.0+wasi-snapshot-preview1
	wasm-bindgen-backend@0.2.93
	wasm-bindgen-macro-support@0.2.93
	wasm-bindgen-macro@0.2.93
	wasm-bindgen-shared@0.2.93
	wasm-bindgen@0.2.93
	wayland-sys@0.31.4
	which@4.4.2
	which@6.0.2
	winapi-i686-pc-windows-gnu@0.4.0
	winapi-util@0.1.9
	winapi-wsapoll@0.1.2
	winapi-x86_64-pc-windows-gnu@0.4.0
	winapi@0.3.9
	windows-core@0.52.0
	windows-core@0.57.0
	windows-implement@0.48.0
	windows-implement@0.57.0
	windows-interface@0.48.0
	windows-interface@0.57.0
	windows-result@0.1.2
	windows-sys@0.48.0
	windows-sys@0.52.0
	windows-sys@0.59.0
	windows-targets@0.48.5
	windows-targets@0.52.6
	windows@0.39.0
	windows@0.48.0
	windows@0.57.0
	windows_aarch64_gnullvm@0.48.5
	windows_aarch64_gnullvm@0.52.6
	windows_aarch64_msvc@0.39.0
	windows_aarch64_msvc@0.48.5
	windows_aarch64_msvc@0.52.6
	windows_i686_gnu@0.39.0
	windows_i686_gnu@0.48.5
	windows_i686_gnu@0.52.6
	windows_i686_gnullvm@0.52.6
	windows_i686_msvc@0.39.0
	windows_i686_msvc@0.48.5
	windows_i686_msvc@0.52.6
	windows_x86_64_gnu@0.39.0
	windows_x86_64_gnu@0.48.5
	windows_x86_64_gnu@0.52.6
	windows_x86_64_gnullvm@0.48.5
	windows_x86_64_gnullvm@0.52.6
	windows_x86_64_msvc@0.39.0
	windows_x86_64_msvc@0.48.5
	windows_x86_64_msvc@0.52.6
	winnow@0.6.18
	winreg@0.10.1
	winsafe@0.0.19
	winver@1.0.0
	wmi@0.12.2
	x11rb-protocol@0.12.0
	x11rb@0.12.0
"

inherit cargo

DESCRIPTION="A simple fetch utility written in Rust"
HOMEPAGE=""
SRC_URI="
	https://gitlab.com/Alxhr0/nexusfetch/-/archive/${PV}/nexusfetch-${PV}.tar.gz
	${CARGO_CRATE_URIS}
"


LICENSE="GPL-3"
# Dependent crate licenses
LICENSE+=" Apache-2.0 BSD GPL-3 ISC MIT MPL-2.0 Unicode-DFS-2016"
SLOT="0"
KEYWORDS="~amd64"
