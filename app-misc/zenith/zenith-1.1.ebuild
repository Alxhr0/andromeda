EAPI=8

inherit go-module

DESCRIPTION="A gentoo system manager"
HOMEPAGE=""
SRC_URI="
	https://gitlab.com/Alxhr0/zenith/-/archive/${PV}/zenith-${PV}.tar.gz
"

LICENSE="MIT"
SLOT="0"

KEYWORDS="~amd64"

src_compile() {
    ego build
}

src_install() {
    dobin zenith

    default
}
