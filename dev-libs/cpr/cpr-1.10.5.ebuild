# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="C++ Requests: Curl for People, a spiritual port of Python Requests."
HOMEPAGE="https://github.com/libcpr/cpr"
if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/libcpr/cpr.git"
else
  SRC_URI="https://github.com/libcpr/cpr/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
  KEYWORDS="~amd64"
fi

IUSE="+ssl boost"

RDEPEND="
	net-misc/curl
	ssl? ( dev-libs/openssl )
	boost? ( dev-libs/boost )
"

DEPEND="
	${RDEPEND}
"

BDEPEND="
	virtual/pkgconfig
"

LICENSE="MIT"
SLOT="0"

src_configure() {
	local mycmakeargs=(
		-DCPR_USE_SYSTEM_CURL=true
		-DCPR_ENABLE_SSL=$(usex ssl)
		-DCPR_FORCE_OPENSSL_BACKEND=$(usex ssl)
		-DCPR_USE_BOOST_FILESYSTEM=$(usex boost)
	)
	cmake_src_configure
}
