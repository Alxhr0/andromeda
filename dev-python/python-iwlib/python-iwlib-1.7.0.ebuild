# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{11..13} )

inherit distutils-r1

DESCRIPTION="Python bindings for Wireless Tools (libiw)"
HOMEPAGE="https://github.com/nhoad/python-iwlib"
SRC_URI="https://github.com/nhoad/python-iwlib/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
    net-wireless/wireless-tools
    dev-lang/python
"
DEPEND="${RDEPEND}"
BDEPEND="dev-python/setuptools"

S="${WORKDIR}/${PN}-${PV}"

distutils_enable_tests pytest

src_prepare() {
    default
}

src_install() {
    distutils-r1_src_install
}

