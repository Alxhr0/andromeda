# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	autocfg@1.1.0
	bitflags@1.3.2
	clap@3.1.2
	clap_derive@3.1.2
	hashbrown@0.11.2
	heck@0.4.0
	indexmap@1.8.0
	itoa@1.0.1
	lazy_static@1.4.0
	memchr@2.4.1
	os_str_bytes@6.0.0
	proc-macro-error@1.0.4
	proc-macro-error-attr@1.0.4
	proc-macro2@1.0.36
	quote@1.0.15
	ryu@1.0.9
	serde@1.0.136
	serde_derive@1.0.136
	serde_json@1.0.79
	swayipc@3.0.0
	swayipc-types@1.0.1
	syn@1.0.86
	textwrap@0.14.2
	thiserror@1.0.30
	thiserror-impl@1.0.30
	unicode-xid@0.2.2
	version_check@0.9.4
"

inherit cargo

DESCRIPTION="Autotiling for sway (and possibly i3)"
HOMEPAGE="https://github.com/ammgws/autotiling-rs"
if [[ ${PV} == 9999 ]]; then
    inherit git-r3
    EGIT_REPO_URI="https://github.com/ammgws/autotiling-rs.git"
else
    SRC_URI="https://github.com/ammgws/autotiling-rs/archive/refs/tags/v${PV}.tar.gz
           ${CARGO_CRATE_URIS}
  "
  KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="
  >=virtual/rust-1.70.0
"

QA_FLAGS_IGNORED="usr/bin/${PN}"

src_unpack() {
  git-r3_src_unpack
  cargo_live_src_unpack
}

src_configure() {
  cargo_src_configure
}

src_install() {
  cargo_src_install
}
