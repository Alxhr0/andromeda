# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="KDE service menus for image file processing"
HOMEPAGE="https://github.com/marco-mania/kf6-servicemenus-imagetools"
SRC_URI="https://github.com/marco-mania/kf6-servicemenus-imagetools/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
  dev-qt/qttools:6
  media-gfx/optipng
  media-libs/libjxl
  media-libs/exiftool
  media-gfx/imagemagick
  kde-apps/kdialog
  media-gfx/pngquant

"
RDEPEND="${DEPEND}"
S="${WORKDIR}/kf6-servicemenus-imagetools-5"

src_install() {
  insinto "${DESTDIR}/usr/share/kio/servicemenus"
  doins ${S}/servicemenus/*

  eapply "${FILESDIR}/rename_cjpegli_to_cjpeg.patch"

  exeinto "$DESTDIR/usr/bin"
  doexe ${S}/bin/*
}
