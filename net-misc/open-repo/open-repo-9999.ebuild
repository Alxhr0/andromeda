# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Open git repo straight from your terminal"
HOMEPAGE="https://github.com/alphatechnolog/open-repo"
EGIT_REPO_URI="https://github.com/alphatechnolog/open-repo.git"

SLOT="0"
RDEPEND="virtual/zig"

src_compile() {
	zig build -Doptimize=ReleaseSafe
}

src_install() {
	insinto /usr/bin
	dobin zig-out/bin/open-repo
}
