
EAPI=8

inherit multilib-build

DESCRIPTION="Virtual for Zig language compiler"

SLOT="0/llvm-19"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

RDEPEND="|| (
	~dev-lang/zig-bin-${PV}
	~dev-lang/zig-${PV}
)"
