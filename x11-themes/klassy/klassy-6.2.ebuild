# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

MY_PV=$(ver_rs 2 '.breeze')
DESCRIPTION="Highly customizable theme plugin for KDE Plasma desktop"
HOMEPAGE="https://github.com/paulmcauley/klassy"
SRC_URI="https://github.com/paulmcauley/klassy/archive/refs/tags/${MY_PV}.breeze${MY_PV}.1.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-1"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	kde-plasma/breeze:6
	kde-frameworks/frameworkintegration:6
	x11-themes/hicolor-icon-theme
	kde-plasma/kdecoration:6
	kde-frameworks/kirigami:6
	kde-plasma/kwayland:6
	kde-frameworks/extra-cmake-modules
"
RDEPEND="${DEPEND}"
BDEPEND="kde-frameworks/kcmutils:6"

S="${WORKDIR}/klassy-${PV}.breeze${MY_PV}.1"
